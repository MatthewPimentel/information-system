/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Institute;
import java.util.*;

/**
 *
 * @author Matthew
 */
public class Institute {
    private String name;
    private ArrayList<Department> department;
    
    public Institute(String name, ArrayList<Department> department){
        this.name = name;
        this.department = department;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
    public void setDepartment(ArrayList<Department> department){
        this.department = department;
    }
    
    public ArrayList<Department> getdepartment(){
        return department;
    }
    
    public int getNumberOfStudents(){
        int nostudents = 0;
        List <Student> students;
        for (Department dept : department){
            students = dept.getStudent();
            
            for (Student s : students){
                nostudents++;   
            }
        }
        return nostudents;
    }
    
}
