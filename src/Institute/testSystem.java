/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Institute;
import java.util.*;
/**
 *
 * @author Matthew
 */
public class testSystem {
    public static void main (String[] args){
        Student s1 = new Student("Matt", 3452);
        Student s2 = new Student("Megan", 2442);
        Student s3 = new Student("Tom", 6422);
        Student s4 = new Student("Adam", 1352);
        Student s5 = new Student("Becky", 32352);
        
        ArrayList<Student> student = new ArrayList<Student>();
        student.add(s1);
        student.add(s2);
        student.add(s3);
        
        ArrayList<Student> student2 = new ArrayList<Student>();
        student2.add(s4);
        student2.add(s5);
        
        Department d1 = new Department("CSS", student);
        Department d2 = new Department("ECE", student2);
        
        ArrayList<Department> department = new ArrayList<Department>();
        department.add(d1);
        department.add(d2);
        
        Institute institute = new Institute("Sheridan",department);
        System.out.println(institute.getNumberOfStudents());
        
        
        
        
        
    }
}
