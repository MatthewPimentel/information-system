/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Institute;
import java.util.*;
/**
 *
 * @author Matthew
 */
public class Department {
    private String name;
    private ArrayList<Student> student;
    
    public Department(String name, ArrayList <Student> student){
        this.name = name;
        this.student = student;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
    public void setStudent(ArrayList<Student> student){
        this.student = student;
    }
    
    public ArrayList<Student> getStudent(){
        return student;
    }
}
